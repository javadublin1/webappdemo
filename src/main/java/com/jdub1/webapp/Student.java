package com.jdub1.webapp;

import lombok.*;

@Data
// getter
// setter
// equals and hashcode
// required args constructor
// to string

@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private String imie;
    private String nazwisko;
}
