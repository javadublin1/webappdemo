<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 8/4/19
  Time: 12:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Multiply</title>
    <link href="css/style.css">
</head>
<body>

<form action="multiplicationTable.jsp" method="post">
    <label for="rX">RozmiarX :</label>
    <input id="rX" name="rozmiarX" type="number" value="0" min="0"/>
    </br>
    <%--Powyżej stworzona etykieta oraz jedno pole tekstowe do wypełnienia przez użytkownika--%>
    <label for="rY">RozmiarY :</label>
    <input id="rY" name="rozmiarY" type="number" value="0" min="0"/>
    </br>
    <%----%>
    <input type="submit"><%--Guzik "wyślij formularz" --%>
</form>


<hr>
<table>
    <%
        String rozmiarX = request.getParameter("rozmiarX");
        int rozmiarx;
        if (rozmiarX != null) {
            rozmiarx = Integer.parseInt(rozmiarX);
        } else {
            rozmiarx = 10;
        }

        String rozmiarY = request.getParameter("rozmiarY");
        int rozmiary;
        if (rozmiarY != null) {
            rozmiary = Integer.parseInt(rozmiarY);
        } else {
            rozmiary = 10;
        }

        for (int i = 0; i < rozmiary; i++) {
            out.println("<tr>");
            for (int j = 0; j < rozmiarx; j++) {
                out.println("<td>");

                out.print((i + 1) * (j + 1));

                out.println("</td>");
            }
            out.println("</tr>");
        }
    %>
</table>


</body>
</html>
