package com.jdub1.webapp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/add")
public class StudentFormServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        Chcemy załadować widok (załadować jsp)
        req.getRequestDispatcher("/studentForm.jsp").forward(req, resp);
//        linia wyżej ładuje nasz formularz JSP
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String imie = req.getParameter("formImie");
        String nazwisko = req.getParameter("formNazwisko");

        Student student = new Student(imie, nazwisko);

        StudentListServlet.studentList.add(student);

        resp.sendRedirect("/list");
    }
}
