<%@ page import="com.jdub1.webapp.StudentListServlet" %>
<%@ page import="com.jdub1.webapp.Student" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 8/4/19
  Time: 2:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>StudentList</title>
</head>
<body>
<table>
    <%
        for (Student student : StudentListServlet.studentList) {
            out.print("<tr>");
            out.print("<td>" + student.getImie() + "</td>");
            out.print("<td>" + student.getNazwisko() + "</td>");
            out.print("</tr>");
        }
    %>
</table>
</body>
</html>
